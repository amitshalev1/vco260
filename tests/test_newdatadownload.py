import os
os.chdir('/mnt/wrkdir/vco260/')
from src.new_datadownload import *

def test_datadownload():
    #Download the data
    df=download_all()
    assert set(crops_dict.values()).difference(df['Organism ID'].unique())==set(),"some crops are missing"
    
    #COUNT how many total genes downloaded before merge
    uniprot = download_uniprot(list(crops_dict.values()),from_cache=True)
   
    ncbi=download_ncbi(crops_dict,from_cache=True)
    exp=download_expression(exp_crops_dict,from_cache=True)
    merged=merge_downloaded(uniprot,ncbi,exp)    
    
    print('number of uniprot genes: ', uniprot.shape[0])
    print('number of uniprot genes after dropping without gene id: ', uniprot.dropna(subset=['Entry','Cross-reference (GeneID)']).shape[0])    
    print('number of genes after merge:',merged.Entry.nunique())    
    assert merged.Entry.nunique()==uniprot.dropna(subset=['Entry','Cross-reference (GeneID)']).shape[0],'inconsistency between number of genes from uniprot and merged'
    
    
    print('number of columns in uniprot+ncbi+exo: ',uniprot.shape[1]+ncbi.shape[1]+exp.shape[1])
    print('number of columns in merged: ',merged.shape[1])
    
    
    exp_file=path+'exp_download_all.parquet'
    ncbi_file=path+'ncbi_download_all.parquet'
    uniprot_file=path+'uniprot_download_all.parquet'
    merged_file=path+'download_all.parquet'
    
    text_file = open("data/summary.txt", "w")
    
    #A list of the exact path of each downloaded file (including on which machine).
    text_file.write("uniprot data file: %s\n" % uniprot_file)  
    text_file.write("gene data file: %s\n" % ncbi_file)
    text_file.write("expression data file: %s\n" % exp_file)    
    text_file.write("non nan per column: %s\n" % str(merged.shape[0]-merged.isna().sum()))
    text_file.write("number of unique per column: %s\n" % str(merged.nunique()))    
    text_file.write("merged data file: %s\n" % merged_file)
    
    to_write='''
    import os
    os.chdir('/mnt/wrkdir/vco260/')
    from src.new_datadownload import *
    df=download_all()'''  
    text_file.write("example use: \n%s" % to_write)    
    text_file.close()    