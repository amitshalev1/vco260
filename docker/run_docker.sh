docker build -t amitshalev/vco260_gpu - < gpu/Dockerfile
if docker inspect -f {{.State.Running}} gd_gpu; then 
    docker stop gd_gpu && docker system prune -f && nvidia-docker run -it --name=gd_gpu -v /home/anat.lavi.itzkovitz\@FromEarthToLife.net/vco260/:/vco260/ -v /mnt/:/mnt/ -p 8888:8888 amitshalev/vco260_gpu jupyter notebook --no-browser --ip=0.0.0.0 --allow-root --notebook-dir=/ --NotebookApp.token=''
    fi
else
    nvidia-docker run -it --name=gd_gpu -v /home/anat.lavi.itzkovitz\@FromEarthToLife.net/vco260/:/vco260/ -v /mnt/:/mnt/ -p 8888:8888 amitshalev/vco260_gpu jupyter notebook --no-browser --ip=0.0.0.0 --allow-root --notebook-dir=/ --NotebookApp.token=''