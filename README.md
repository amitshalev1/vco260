# README for vco260 new git#

# goal:
# write instructions that anyone can understand how to run docker with all the code and neccesary python packages:

### getting started ###


1. git clone repositry (look for clone button in this page)
2. cd into git folder/docker (cd vco260/docker)
3. bash run_docker.sh 
4. run nvidia-smi to understand if server has gpu (or checkout the output of run_docker.sh)
5. switch 8888 left of the : symbol if 8888 is taken and put path to git clone folder instead of git_folder
6. if gpu run :

-  nvidia-docker run -it --name=gd_gpu -v /git_folder/:/vco260/ -p 8888:8888 amitshalev/vco260_gpu jupyter notebook --no-browser --ip=0.0.0.0 --allow-root --notebook-dir=/ --NotebookApp.token=''
-  if cpu run:
-  docker run -it --name=gd_gpu -v /git_folder/:/vco260/ -p 8888:8888 amitshalev/vco260_cpu jupyter notebook --no-browser --ip=0.0.0.0 --allow-root --notebook-dir=/ --NotebookApp.token=''

- notice the container name. you can check open containers with docker ps

7. to download the data:
- python -c "from src.datadownload import *; Gene(),Uniprot(),Expression()" #will download(first time only) and print summary of data downloaded

