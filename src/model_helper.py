import tensorflow as tf
from keras.layers import Dense, Input, Embedding, BatchNormalization, Dropout, concatenate, Reshape
from keras.models import Model
from keras.losses import mean_squared_error
import keras.backend as K
import numpy as np
import pandas as pd
from keras import optimizers
from keras.models import model_from_json
from keras.models import Sequential
from keras.callbacks import ModelCheckpoint, EarlyStopping, CSVLogger
import time
from src.clr_callback import CyclicLR
sgd = optimizers.SGD(clipnorm=1.)


def gene_data_prep(gene_data):
    '''returns the X in a ordered way for embedding'''
    cat_cols = [c for c in gene_data.columns if c.endswith('_categorical')]
    num_cols = [c for c in gene_data.columns if c.endswith('_numeric')]    
    x = gene_data[cat_cols+num_cols].values    
    cat_cols = [c for c in gene_data.columns if c.endswith('_categorical')]
    x = [x.T[i] for i in range(len(cat_cols))] + [x[:, len(cat_cols):]]
    return x

def build_inp(gene_data):
    #uses the columns in the gene_data fataframe to build the input shape  
    #for the encoder
    
    emb_cols = [(idx, c) for idx,c in enumerate(gene_data.columns) if c.endswith('_categorical')]
    num_cols = [idx for idx,c in enumerate(gene_data.columns) if c.endswith('_numeric')]
    emb_layers = []
    inputs = []
    
    #creating embedding layers and inputs
    for idx, col in emb_cols:
        categories = gene_data[col].nunique()
        emb_size = min([int(categories/2), 50])
        inp = Input((1,), name = 'inp_emb_%s'%(col.replace(" ", "_")) )
        inputs.append(inp)
        emb = Embedding(min(categories,1), emb_size, name= 'emb_%s'%(col.replace(" ", "_")))(inp)
        emb_layers.append(emb)
    
    #concatenate the embedding layers 
    conctenated = concatenate(emb_layers, name='concatenated_emb_layer')
    #reshape the concat layers so it can be later on be conctanenatd with the numric columns
    conctenated = Reshape((int(conctenated.shape[2]),), name='reshape_embedding')(conctenated)
    
    #creating inputs for the "numeric" columns
    num_inp = Input(shape=(len(num_cols), ), name='inp_numeric')
    inputs.append(num_inp)
    bottleneck_size = min(3000, int(len(num_cols)/2))
    #push all the numeric columns trough a "bottleneck" of  neurons, to reduce the dimentionality
    x = Dense(bottleneck_size, activation='relu')(num_inp)
    conctenated = concatenate([conctenated] + [x])
    return conctenated, inputs

def encoder_deep_wide(gene_data, architecture, dropout, batchnorm, use_skip=False):
    #create one "arm" of the encoder
    con, inputs = build_inp(gene_data)
    for idx, width in enumerate(architecture):
        if idx == 0:
            x = Dense(width, activation='relu', name='dense_%s'%(idx))(con)
        else:
            x = Dense(width, activation='relu')(x)
        if dropout: x = Dropout(dropout)(x)
        if batchnorm: x = BatchNormalization()(x)
    
    #add a "skip connection..."
    if use_skip:
        skip = Dense(architecture[0], activation='relu', name='skipconnection')(con)
        x = concatenate([skip, x], name='concat_skip_deep')
    x = concatenate([con, x], name='deep_and_shallow')
    return x, inputs



def get_model(gene_data,emb_size=300 ,architecture = [2000, 2000, 2000, 2000, 2000], dropout=0.3, batchnorm=True,
              optimizer = sgd,
              loss = mean_squared_error,
              metrics = ['accuracy'],
              use_skip = False):
    
    x, inputs = encoder_deep_wide(gene_data, architecture, dropout, batchnorm, use_skip)
    last_layer = Dense(300, name='last_layer')(x)
    
    model = Model(inputs=inputs, outputs=last_layer)
    model.compile(optimizer=optimizer, loss=loss)
    return model