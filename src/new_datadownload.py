import os
import pandas as pd
import requests
from tqdm import tqdm as tqdm
import numpy as np
from io import StringIO
os.chdir('/mnt/wrkdir/vco260/')


path='data/'
columns="""id,
genes,
organism-id,
comment(FUNCTION),
keywords,
features,
feature(ACTIVE%20SITE),
feature(BINDING%20SITE),
feature(CALCIUM%20BIND),
comment(CATALYTIC%20ACTIVITY),
feature(DNA%20BINDING),
comment(ABSORPTION),
comment(ACTIVITY%20REGULATION),
comment(COFACTOR),
ec,
comment(KINETICS),
feature(METAL%20BINDING),
feature(NP%20BIND),
comment(PATHWAY),
comment(PH%20DEPENDENCE),
comment(REDOX%20POTENTIAL),
rhea-id,
feature(SITE),
comment(TEMPERATURE%20DEPENDENCE),
comment(TISSUE%20SPECIFICITY),
comment(INDUCTION),
comment(DEVELOPMENTAL%20STAGE),
comment(TISSUE%20SPECIFICITY),
comment(INDUCTION),
comment(DEVELOPMENTAL%20STAGE),
comment(DISRUPTION%20PHENOTYPE),
comment(INVOLVEMENT%20IN%20DISEASE),
feature(MUTAGENESIS),
database(GeneID),
database(Gramene),
database(PANTHER),
database(BioCyc),
database(UniPathway),
database(OrthoDB),
database(Pfam),
database(ExpressionAtlas),
database(Genevisible),
database(CollecTF)""".replace('\n','')

crops_dict={'Watermelon': 3654,
 'Muskmelon': 3656,
 'Corn':4577}

exp_crops_dict={'Watermelon': 'Citrullus lanatus',
 'Muskmelon': 'Cucumis melo',
 'Corn':'Zea mays' }

organisms = list(crops_dict.values())


def download_uniprot(organisms:list = organisms,
                     columns = columns,
                     limit = 10000000, 
                     fname=path+'uniprot_download_all.parquet', 
                     from_cache=True):
    '''
    fetch a combined large file of all the data associated in uniprot with 
    specific organisms. 
    The organisms are entered as a list of taxonomy IDs
    '''
    if not from_cache or not os.path.isfile(fname):
        print('downloading...')
        organisms = ' OR '.join([f'taxonomy:{o}' for o in organisms])
        params = f'&format=tab&limit={limit}&columns={columns}'
        url = f'https://www.uniprot.org/uniprot/?query={organisms}{params}'
        download_file(url, 'uniprot_download_all.tab')
        uniprot =  pd.read_csv('uniprot_download_all.tab', sep='\t')
        uniprot.to_parquet(fname)
        return uniprot
      
    return pd.read_parquet(fname)


def download_file(url, local_filename):
    ''' an helper function to download large files in chuncks and show progress'''
    with requests.get(url, stream=True) as r:
        r.raise_for_status()
        with open(local_filename, 'wb') as f:
            for chunk in tqdm(r.iter_content(chunk_size=1000000)): 
                if chunk: # filter out keep-alive new chunks
                    f.write(chunk)
    return local_filename


def ncbigetsummary(db,query,webenv,st):
    url='https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db='+db+'&retstart='+str(st)+'&retmax=500&retmode=json&query_key='+query+'&WebEnv='+webenv
    req=requests.get(url)
    if req.status_code==200 and req.text!="":
        return req.json()
    print(req.status_code)


def ncbigetcrop(db,organism):
    '''
    returns a json to be used in ncbigethist and ncbiloopfetch/summary
    '''

    url='https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db='+db + '&term=txid' + str(organism) + '[Organism]&usehistory=y&retmax=1000000600&retmode=json'
    req=requests.get(url)
    if req.status_code==200 and req.text!="":
        return req.json()
    print(req.status_code)

def ncbigethist(query_result):
    return query_result['esearchresult']['querykey'] ,query_result['esearchresult']['webenv'] ,query_result['esearchresult']['idlist']


def get_ncbi_gene_crop(tax_id,crop_name,db='gene'):
    #print(directory)
    query,webenv,id_list=ncbigethist(ncbigetcrop(db,tax_id))
    num=len(id_list)
    frames=[ncbigetsummary(db,query,webenv,x) for x in tqdm(np.arange(int(num/500)+2)*500)]
    temp=[]
    for i in range(len(frames)):
        if 'result' in list(frames[i].keys()):
            temp.append(pd.DataFrame(pd.DataFrame(frames[i])[:-3]['result'].values.tolist()))

    return pd.concat(temp)

    
def download_ncbi(organisms:dict = crops_dict,
                     fname=path+'ncbi_download_all.parquet', 
                     from_cache=True):
    '''
    fetch a combined large file of all the data associated in uniprot with 
    specific organisms. 
    The organisms are entered as a list of taxonomy IDs
    '''
    frames=[]
    if not from_cache or not os.path.isfile(fname):    
        for k,v in organisms.items():
            frames.append(get_ncbi_gene_crop(v,k))

        ncbi=pd.concat(frames)
        ncbi.to_csv(fname)
        return ncbi
      
    return pd.read_csv(fname)        




def array_express(crop):

    #print(ogr)
    req=requests.get('https://www.ebi.ac.uk/arrayexpress/json/v3/experiments?species='+crop+'&gxa=True')
    if req.status_code==200:
        data=pd.DataFrame(req.json())
        if 'experiment' not in data.T.columns.tolist():
            print('no experiment for '+crop)
            return 'no',[]
        array_express1=pd.DataFrame(data.T.experiment.values[0]) 
        entry_list=array_express1.accession.values.tolist()
        return array_express1,entry_list
    print(req.status_code)


def crop_array_express(crop):
    #print(directory)
    array_express1,entry_list=array_express(crop)
    p1='https://www.ebi.ac.uk/gxa/experiments-content/'
    #u2='/resources/ExperimentDownloadSupplier.Microarray/query-results'
    u2='/download/RNASEQ_MRNA_BASELINE'
    #p2='/download/RNASEQ_MRNA_DIFFERENTIAL'
    reqs=[requests.get(p1+ x+ u2).text for x in tqdm(entry_list)]
    frames=[]
    for i in tqdm(range(len(reqs))):
        gene_id=reqs[i].find('Gene ID')
        if gene_id!=-1:
            frames.append(pd.read_table(StringIO(reqs[i][gene_id:]),sep='\t',error_bad_lines=False))
        else:
            print('fail')
    if frames!=[]:
        return merge_all_expression_dfs_list(frames)
    else:
        return pd.DataFrame()


def merge_all_expression_dfs_list(df_list):
    all_genes = set([c  for d in df_list for c in d['Gene ID'].values])
    base = pd.DataFrame(list(all_genes), columns=['Gene ID'])
    oriented_df = []
    for df in tqdm(df_list):
        df = df.drop_duplicates('Gene ID')
        new_df = pd.merge(base, df, how='left', on='Gene ID', sort=False)      
        #new_df.index = new_df['Gene ID']
        to_drop = list(set(['Gene ID', 'Gene Name', 'Design Element']).intersection(set(new_df.columns)))
        new_df.drop(columns=to_drop, inplace=True)
        new_df = new_df.astype(np.float32)
        oriented_df.append(new_df)

    oriented_df = pd.concat(oriented_df, axis=1)
    oriented_df.index = base['Gene ID']
    return oriented_df

def download_expression(organisms:dict = exp_crops_dict,
                     fname=path+'exp_download_all.parquet', 
                     from_cache=True):
    
    frames=[]
    if not from_cache or not os.path.isfile(fname):    
        for v in organisms.values():
            frames.append(crop_array_express(v))


        exp=pd.concat(frames).dropna(how='all',axis=1)
        exp.to_csv(fname)
        return exp
      
    return pd.read_csv(fname) 


def merge_downloaded(uniprot,ncbi,exp,gene_map='data/uni_to_ensemble.csv'):
    '''loads all the data and merges into one big table'''


    uniprot = uniprot.dropna(subset=['Entry', 'Cross-reference (GeneID)'])
    uniprot['Cross-reference (GeneID)'] = uniprot['Cross-reference (GeneID)'].str.split(';').apply(lambda l: l[0])
    uniprot['Cross-reference (GeneID)']= uniprot['Cross-reference (GeneID)'].astype('int64')
    ncbi = ncbi.dropna(subset=['uid'])

    
    #merge gene and uniprot
    merged = pd.merge(uniprot, ncbi, how='left', left_on='Cross-reference (GeneID)', right_on='uid')



    #add the expression data
    uni_map = pd.read_csv(gene_map)
    uni_map.columns = ['idx', 'Entry','source', 'ensembl_id']
    uni_map['ensembl_id'] = uni_map['ensembl_id'].str.lower()

    
    merged = pd.merge(merged,uni_map[['Entry', 'ensembl_id']], how='left', right_on='Entry', left_on='Entry')
    exp['Gene ID']=exp['Gene ID'].str.lower()
    merged = pd.merge(merged,exp, how='left', right_on='Gene ID', left_on='ensembl_id')
    
    return merged.dropna(how='all',axis=1)


def download_all(exp_crops_dict:dict = exp_crops_dict,
                crops_dict:dict = crops_dict,                 
                     fname=path+'download_all.parquet', 
                     from_cache=True):
    '''
    exp_crops_dict - dict with common name : latin name
    crops_dict - dict with common name : tax id
    '''
    if not from_cache or not os.path.isfile(fname):    
        uniprot = download_uniprot(list(crops_dict.values()),from_cache=from_cache)
        ncbi=download_ncbi(crops_dict,from_cache=from_cache)
        exp=download_expression(exp_crops_dict,from_cache=from_cache)
        merged=load_raw_gene_data(uniprot,ncbi,exp)
        
        merged.to_parquet(fname)
        return merged
      
    return pd.read_parquet(fname)