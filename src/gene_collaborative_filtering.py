from src.dataprocess import *
from keras.layers import Input, Embedding, concatenate, Reshape, Dense, Dropout, BatchNormalization
from keras.models import Model
import matplotlib.pyplot as plt
from keras.losses import binary_crossentropy
from keras.callbacks import ModelCheckpoint, EarlyStopping, CSVLogger
import time
from keras import optimizers
from src.clr_callback import CyclicLR
from src.clr import LRFinder
import keras.backend as K

def mean_pred(y_true, y_pred):
    return K.mean(y_pred)

def binary_accuracy_custom(y_true, y_pred):
    return K.mean(K.equal(K.round(y_true), K.round(y_pred)), axis=-1)

def uniprot_go_and_pmid_data(organism):
    
    '''
    download data using datadownload or test_datadownload
    '''
    
    
    pass

def filter_out_rare(df,cut_off=5,col_name='go',ind='Entry'):
    ''' 
    filters out go terms that have less then cut_off number of genes associated
    '''
    col1=col_name+'_categorical'
    col2=ind+'_categorical'
    gb=df.groupby(col1).count().sort_values(col2,ascending=False)
    go_to_use=gb[gb[col2]>cut_off].index.values
    return df[df[col1].isin(go_to_use)]
    
    
       
def rename_categoric_numeric_columns(df):
    '''
    turns any table to integer represented values table and returns conversion codes
    '''  
    categorical = {c : '%s_categorical'%(c) for c in df.columns if df[c].dtype == 'object' }
    numeric = {c : '%s_numeric'%(c) for c in df.columns if df[c].dtype != 'object'}
    df = df.rename(columns = numeric)
    df = df.rename(columns = categorical)   
    cat_cols = [c for c in df.columns if c.endswith('_categorical')]
    cat_codes = []
    for c in  tqdm(cat_cols):
        df[c], mapping_index = pd.Series(df[c]).factorize()
        cat_codes.append(mapping_index) 
    return df,cat_codes


#---------------------
def get_uniprot_colab_data(col,cut_off_idx_per_col=5,cut_off_col_per_idx=1,split_on=';',redo=False):
    '''
    combined function for the creation of collaborative filtering data from download proteins and gene data
    1. load all data and merge into one table(dataframe)
    2. reduce to 2 columns(id and the chosen column)
    3. split chosen column if on split_on(used to seperate values where each protein entry has more the one value
    4. filter out go terms with less the the cutoff number of proteins
    
    '''
    merged=load_raw_gene_data(redo=redo)
    merged_df=pd.read_parquet(merged)
    df=merged_df[['Entry',col]]
    if split_on!=None:
        df=expand_list(df,col,'Entry',split_on)
    df,codes=rename_categoric_numeric_columns(df)
    df=filter_out_rare(df,cut_off_idx_per_col,col) #filter go terms with less then 5 genes
    df=filter_out_rare(df,cut_off_col_per_idx,col_name='Entry',ind=col) # filter genes with less then 5 go terms
    return df,codes
#---------------------
def negative_sampling(df,batch_size=256):
    '''
    creates negative sample(pairs of protein id and go term id the are not related
    randomly samples gene and includes the go terms that it does not have
    
    '''
    
    go_max=df.go_categorical.max()
    temp1=pd.DataFrame()
    while temp1.shape[0]<batch_size:
        temp=pd.DataFrame()
        ent=df.Entry_categorical.sample(1).values[0]
        indexed_not_in=list(set(np.arange(go_max)).difference(df[df.Entry_categorical==ent].go_categorical.values))
        temp['go_categorical']=np.random.choice(indexed_not_in,size=min(batch_size,len(indexed_not_in)))
        temp['target']=0
        temp['Entry_categorical']=ent
        temp1=temp1.append(temp[['Entry_categorical','go_categorical','target']])
    return temp1.sample(batch_size)

def get_original(original_col,num,codes):
    
    '''
    helper function to return original values from codes produced by rename_categoric_numeric_columns
    '''
    if original_col=='Entry':
        return codes[0][num]
    else: return codes[1][num]
        
def test_conversion(processed_df,original_df,codes,id_col='Entry',val_col='go',verbose=False):
    '''random sample from processed_df and original and compare with codes conversion to verify validity'''
    num=processed_df[id_col+'_categorical'].sample(1).values[0] # sample go term from processed dataframe
    if verbose:print('sampled ' +id_col +' number:' +str(num))
    ent=get_original(id_col,num,codes) #get original value
    if verbose:print('value of ' + str(id_col) +  ' number in original table is ' +ent)
    go_num=np.random.choice(processed_df[processed_df[id_col+'_categorical']==num][val_col+'_categorical'].values.tolist())
    if verbose:print('number of '+val_col+' in processed table is ' +str(go_num))
    orig=get_original(val_col,go_num,codes)
    mystr=original_df[original_df[id_col]==ent][val_col].values[0]
    if verbose: print('checking if ' +orig+' is in original string: '+mystr)
    return orig  in mystr



def test_intersection(train_file='data/training_data.parquet',test_file='data/test_data.parquet'):
    con=True
    train_data=pd.read_parquet(train_file)
    test_data=pd.read_parquet(test_file)
    for ent in tqdm(test_data.Entry_categorical.values):
        in_test=set(test_data[test_data.Entry_categorical==ent].go_categorical.values.tolist())
        if not set(train_data[train_data.Entry_categorical==ent].go_categorical.values.tolist()).intersection(in_test) ==set():
            print (ent)
        con=con and set(train_data[train_data.Entry_categorical==ent].go_categorical.values.tolist()).intersection(in_test) ==set()
    print(con)

def train_test_split(df,sample_on='go_categorical',test_size=100,sample_per=3,data_name=''):
    gterms=np.random.choice(df[sample_on].unique(),size=test_size,replace=False)
    idx=[]
    for g in gterms:
        idx+=df[df[sample_on]==g].sample(sample_per).index.values.tolist()
    df[df.index.isin(idx)].to_parquet('data/test_data'+data_name+'.parquet')
    df[~df.index.isin(idx)].to_parquet('data/training_data'+data_name+'.parquet')

def collab_model(df,emb_cols,
                 batchnorm=True,
                 use_skip=False,
                 n_features = 300,
                 dense_size = [100, 100], 
                 dropout = 0.3,
                 optimizer = 'adam',
                 loss='binary_crossentropy',
                 metrics = [binary_accuracy_custom]
                ):
    '''
    creates a keras model
    
    '''    
    layers=[]
    inputs=[]
    for col in emb_cols:
        inp = Input(shape=(1,), name='input_%s'%(col))
        emb = Embedding(df[col].max(), int(n_features), input_length=1, name='embedd_%s'%(col))(inp)
        inputs.append(inp)
        layers.append(emb)
    
    con = concatenate(inputs = [l for l in layers], name='concat_embeddings')
    con = Reshape((int(con.shape[2]),), name='reshape_embedding')(con)
    for idx, width in enumerate(dense_size):
        if idx == 0:
            x = Dense(width, activation='relu', name='dense_%s'%(idx))(con)
        else:
            x = Dense(width, activation='relu')(x)
        if dropout: x = Dropout(dropout)(x)
        if batchnorm: x = BatchNormalization()(x)
    
    #add a "skip connection..."
    if use_skip:
        skip = Dense(architecture[0], activation='relu', name='skipconnection')(con)
        x = concatenate([skip, x], name='concat_skip_deep')    
    x = Dense(1, activation='sigmoid')(x)
    
    

    model = Model(inputs = inputs, outputs = x)
    model.compile(optimizer = optimizer, loss = loss, metrics = metrics)
    return model


def collab_model2(df,n_features=300,width=100,dropout=0.3,optimizer = 'adam',loss='binary_crossentropy',
                 metrics = ['accuracy',binary_accuracy_custom]):
    '''
    creates a keras model
    
    '''    
    layers=[]
    inputs=[]
    emb_cols=[x for x in df.columns.tolist() if x.endswith('categorical')]
    for col in emb_cols:
        inp = Input(shape=(1,), name='input_%s'%(col))
        emb = Embedding(df[col].max(), int(n_features), input_length=1, name='embedd_%s'%(col))(inp)
        inputs.append(inp)
        layers.append(emb)
    
    con = concatenate(inputs = [l for l in layers], name='concat_embeddings')
    con = Reshape((int(con.shape[2]),), name='reshape_embedding')(con)
    x = Dense(width, activation='relu', name='dense_0')(con)
    x = BatchNormalization()(x)
    x = Dropout(dropout)(x)
    x = Dense(width, activation='relu')(x)
    x = BatchNormalization()(x) 
    x = Dense(1, activation='sigmoid')(x)

    model = Model(inputs = inputs, outputs = x)
    model.compile(optimizer = optimizer, loss = loss, metrics = metrics)
    return model

def data_genrator(df,val_idx, batch_size=32):
    '''
    generator for training data combines half positive half negative
    '''
    pos = 0 # ~position
    while True:
        if pos > (len(df)-batch_size): 
            pos = 0
            #shuffle at each epoch!
            df=df.sample(frac=1)
        temp=df[~df.index.isin(val_idx)][pos:pos+int(batch_size/2)]
        temp['target']=1
        temp=pd.concat([temp,negative_sampling(df,int(batch_size/2))])
        pos += batch_size
        yield [temp.values[:,0],temp.values[:,1]],temp.values[:,2]
        
def data_genrator2(df, batch_size=32):
    '''
    generator for training data combines half positive half negative
    '''
    pos = 0 # ~position
    while True:
        if pos > (len(df)-batch_size): 
            pos = 0
            #shuffle at each epoch!
            df=df.sample(frac=1)
        temp=df[pos:pos+int(batch_size/2)]
        pos += batch_size
        yield [temp.values[:,0],temp.values[:,1]],temp.values[:,2]
        
def get_val_data(df,idx):
    '''
    return validation data(all validation data is positive, is that bad?)
    '''
    temp=df[df.index.isin(idx)]
    return [temp.values[:,0],temp.values[:,1]],temp.values[:,2]

 
def get_val_data2(df):
    '''
    return data ready for model feed
    '''
    temp=df
    return [temp.values[:,0],temp.values[:,1]],temp.values[:,2]

#-----------------------
def train_colab(df_train,df_test,steps=50,batch_size= 100000,minimum_lr=0.0001,maximum_lr=0.04,run_name=''):
    
    '''
    input dataframe composed of 2 integer columns.
    train model with 1-cycle and binary-crossentropy loss 
       returns a trained model and a history of the training.
     '''
    df_train=df_train
    model=collab_model2(df_train)
    #validation data sample and generator
    gen=data_genrator2(df_train,batch_size)
    val_x,val_y=get_val_data2(df_test)    
   

    #hyperparamters
    epoch_size = df_train.shape[0] #number of training examples per epoch. should be larger
    patience = 50 #stop training if the loss not getting lower..
#     step_size = int(0.5 * steps_per_epoch*epochs) #2 steps in cycle
    lr_callback = LRFinder(epoch_size, batch_size,
                           minimum_lr, maximum_lr,
                           lr_scale='exp', save_dir='models/')    

    run_num = 'amit_train_' + time.strftime('%Y_%m_%d_%H_%M')

    #callbacks
    checkpoint = ModelCheckpoint('models/' + run_num + '_cf_take_4_best.h5', save_best_only=True, monitor='loss', save_weights_only=True)
    logger = CSVLogger('models/' + run_num + '_GO_TERM_CLASSIFIER_take_4_log.csv')
    estop = EarlyStopping(monitor='loss', patience = patience)
#     clr = CyclicLR(base_lr=min_lr, max_lr=max_lr, step_size=step_size)
    callbacks = [checkpoint, logger, estop,lr_callback]

    #model
    hist=model.fit_generator(gen, 
                        steps_per_epoch = steps, 
                        epochs = 1, 
                        callbacks = callbacks,
                       validation_data=(val_x,val_y))    

    save_to_pickle((lr_callback),"models/hist"+run_name+".pickle")
    
#-----------------------
def train_colab2(df,idx,steps=1,val_frac=0.001,batch_size= 100000,patience =50,min_lr=0.0001,max_lr=0.3,dropout = 0.3,step_size=10,run_name=''):
    
    '''
    input dataframe composed of 2 integer columns.
    train model with 1-cycle and binary-crossentropy loss 
       returns a trained model and a history of the training.
     '''
    
    model=collab_model2(df.columns.tolist(),True)
    #validation data sample and generator
    gen=data_genrator(df,idx)
    val_x,val_y=get_val_data(df,idx)    


    #hyperparamters
    epoch_size = df.shape[0] -len(idx) #number of training examples per epoch. should be larger
    steps_per_epoch = int(epoch_size / batch_size)
    patience = 50 #stop training if the loss not getting lower..

    

    run_num = 'amit_train_' + time.strftime('%Y_%m_%d_%H_%M')

    #callbacks
    checkpoint = ModelCheckpoint('models/' + run_num + '_cf_take_4_best.h5', save_best_only=True, monitor='loss', save_weights_only=True)
    logger = CSVLogger('models/' + run_num + '_GO_TERM_CLASSIFIER_take_4_log.csv')
    estop = EarlyStopping(monitor='loss', patience = patience)
    clr = CyclicLR(base_lr=min_lr, max_lr=max_lr, step_size=step_size)
    callbacks = [checkpoint, logger, estop, clr]

    #model

    hist=model.fit_generator(gen, 
                        steps_per_epoch = steps_per_epoch, 
                        epochs = epochs, 
                        callbacks = callbacks,
                       validation_data=(val_x,val_y))    

    save_to_pickle((hist,clr.history),"hist"+run_name+".pickle")
        
def train_colab3(df_train,df_test,epochs=50,batch_size= 100000,minimum_lr=0.0001,maximum_lr=0.04,
                 run_name='',
                 prev_model='not a file by default'):
    
    '''
    input dataframe composed of 2 integer columns.
    train model with 1-cycle and binary-crossentropy loss 
       returns a trained model and a history of the training.
     '''
    
    model=collab_model2(df_train)
    #validation data sample and generator
    gen=data_genrator2(df_train,batch_size)
    val_x,val_y=get_val_data2(df_test)    
   

    #hyperparamters
    epoch_size = df_train.shape[0] #number of training examples per epoch. should be larger
    steps_per_epoch = int(epoch_size / batch_size)
    patience = 50 #stop training if the loss not getting lower..
    step_size = int(epoch_size/steps_per_epoch) #2 steps in cycle

    run_num = 'amit_train_' + time.strftime('%Y_%m_%d_%H_%M')

    #callbacks
    checkpoint = ModelCheckpoint('models/' + run_num + '_cf_take_4_best.h5', save_best_only=True, monitor='loss')
    logger = CSVLogger('models/' + run_num + '_GO_TERM_CLASSIFIER_take_4_log.csv')
    estop = EarlyStopping(monitor='loss', patience = patience)
    clr = CyclicLR(base_lr=minimum_lr, max_lr=maximum_lr, step_size=step_size)
    callbacks = [checkpoint, logger, estop,clr]

    if os.path.isfile(prev_model):
        print('loading old run...')
        model.load_weights(prev_model)
        print('loaded ok')
    #model
    print(str(epochs))
    hist=model.fit_generator(gen, 
                        steps_per_epoch = steps_per_epoch, 
                        epochs = epochs, 
                        callbacks = callbacks,
                       validation_data=(val_x,val_y))    

    return model,clr
