import pandas as pd
import os
from tqdm import tqdm
import pickle
import numpy as np


datadir='data'

def save_to_pickle(var,fileadd='picklevar.pickle'):
    with open(fileadd, 'wb') as handle:
        pickle.dump(var, handle)
        

def load_from_pickle(fileadd='picklevar.pickle'):
    with open(fileadd, 'rb') as handle:
        return pickle.load(handle)
    

def expand_list(df,feature,onfeat,split_on=';'):
    '''
    takes dataframe with feature as str column that wil be split by split_on
    '''
    temp=df.dropna(subset=[feature])
    temp[feature]=temp[feature].astype(str).str.lstrip().str.rstrip()
    temp[feature]=temp[feature].str.split(split_on)
    temp=extract_list(temp,feature,onfeat)
    temp[feature]=temp[feature].str.strip()
    return temp.loc[temp[feature]!='']



def n_split(df,feature,onfeat,nsplit,column_list):
    temp1=df.dropna(subset=[feature])
    temp1[feature]=temp1[feature].str.replace('<','').str.strip().str.split(' ',n=nsplit)
    temp=pd.DataFrame()
    temp[column_list]=pd.DataFrame(temp1[feature].values.tolist(), index= temp1[onfeat])
    return temp


#important
def expand(df,feature,onfeature='accession'):  
    '''takes a list of dicts, breaks it down to columns'''
    temp=pd.DataFrame()
    temp=pd.DataFrame(df[feature].values.tolist(),df[onfeature]).add_prefix(feature)
    temp[onfeature]=temp.index  
    return temp


#important
def multy_columns_to_rows(df,feature):
    '''takes multyple columns feature+number and turns it to one column'''
    columns_dict = dict(feature=df.filter(regex="^"+feature).columns)
    df=pd.lreshape(df, columns_dict)
    return df

#good
def extract_list(df,feature,onfeature='accession'):
    '''input a column that is a list of dicts output is column of one dict''' 
    return multy_columns_to_rows(expand(df,feature,onfeature),feature).rename(columns={'feature': feature},inplace=False)

def merge_all_expression_dfs_list(df_list):
    all_genes = set([c  for d in df_list for c in d['Gene ID'].values])
    base = pd.DataFrame(list(all_genes), columns=['Gene ID'])
    oriented_df = []
    for df in tqdm(df_list):
        df = df.drop_duplicates('Gene ID')
        new_df = pd.merge(base, df, how='left', on='Gene ID', sort=False)      
        #new_df.index = new_df['Gene ID']
        to_drop = list(set(['Gene ID', 'Gene Name', 'Design Element']).intersection(set(new_df.columns)))
        new_df.drop(columns=to_drop, inplace=True)
        new_df = new_df.astype(np.float32)
        oriented_df.append(new_df)

    oriented_df = pd.concat(oriented_df, axis=1)
    oriented_df.index = base['Gene ID']
    return oriented_df

def load_from_pickle(fileadd='picklevar.pickle'):
    with open(fileadd, 'rb') as handle:
        return pickle.load(handle)

def clean_df(df):
    df.drop(labels=['Unnamed: 0','Gene ontology (GO).1','Entry.1','Gene names.2','Gene names.1','Mapped PubMed ID.1','PubMed ID.1'],axis=1,inplace=True)

    df.rename(columns={'Domain [FT]':'domainft'},inplace=True)
    df.rename(columns={'Gene ontology (GO)':'go'},inplace=True)
    df.rename(columns={'Gene names  (primary )':'genenames'},inplace=True)
    df.rename(columns={'Gene names  (synonym )':'genenamessyn'},inplace=True)
    df.rename(columns={'Gene names  (ordered locus )':'genenamesord'},inplace=True)
    df.rename(columns={'Gene names  (ORF )':'genenamesorf'},inplace=True)
    df.rename(columns={'Taxonomic lineage (ALL)':'taxlin'},inplace=True)
    df.rename(columns={'Function [CC]':'funccc'},inplace=True)
    df.rename(columns={'Miscellaneous [CC]':'misc'},inplace=True)
    df.rename(columns={'Subunit structure [CC]':'subunitstr'},inplace=True)
    df.rename(columns={'Gene ontology (biological process)':'gobp'},inplace=True)
    df.rename(columns={'Gene ontology (molecular function)':'gomf'},inplace=True)
    df.rename(columns={'Gene ontology (cellular component)':'gocc'},inplace=True)
    df.rename(columns={'Subcellular location [CC]':'subcellularloc'},inplace=True)
    df.rename(columns={'Domain [CC]':'domaincc'},inplace=True)
    df.rename(columns={'Cross-reference (EMBL)':'crossref'},inplace=True)
    df.rename(columns={'Cross-reference (ExpressionAtlas)':'exatlas'},inplace=True)
    df.rename(columns={'Cross-reference (GeneID)':'geneid'},inplace=True)
    df.rename(columns={'Alternative products (isoforms)':'alternativepro'},inplace=True)
    df.rename(columns={'General annotation (ENZYME REGULATION)':'enzyme_reg'},inplace=True)
    df.rename(columns={'Cross-reference (Gramene)':'gramene'},inplace=True)
    
    
  
    return df

def read_all_uniprot_crops(datadir):
    files=[x for x in os.listdir(datadir) if x.endswith('.uniprot') ]
    return clean_df(pd.concat([pd.read_csv(os.path.join(datadir, crop),low_memory=False) for crop in tqdm(files)]))

def read_all_genes_crops(datadir):
    files=[x for x in os.listdir(datadir) if x.endswith('.gene') ]
    return pd.concat([pd.read_csv(os.path.join(datadir,crop),low_memory=False) 
                       for crop in tqdm(files)])

def read_all_exp_crops(datadir):
    files=[x for x in os.listdir(datadir) if x.endswith('.exp') ]
    frames= [load_from_pickle(os.path.join(datadir, crop))  for crop in tqdm(files)]
    return sum(frames,[])

def load_raw_gene_data(datapath=datadir,gene_map='/uni_to_ensemble.csv',redo=False):
    '''loads all the data and merge into one big table'''
    gene_map=datadir+gene_map
    if not os.path.isfile(datapath+'/merged_final.parquet') or redo:
        uniprot = read_all_uniprot_crops(datapath)
        gene = read_all_genes_crops(datapath)
        expdf = read_all_exp_crops(datapath)
        all_cols = set([c  for d in expdf for c in d.columns])
        all_genes = set([c  for d in expdf for c in d['Gene ID'].values])

        #merge expression dataframes

        oriented_df = merge_all_expression_dfs_list(expdf)
        oriented_df.columns = ["%s_%s"%(c,i) for i,c in enumerate(oriented_df.columns)]


        #drop empty and fix types
        uniprot = uniprot.dropna(subset=['Entry', 'geneid'])
        uniprot.geneid = uniprot.geneid.str.split(';').apply(lambda l: l[0])
        uniprot.geneid = uniprot.geneid.astype('int64')
        gene = gene.dropna(subset=['uid'])
        gene.loc[gene['chromosome'].isnull(), ['chromosome']] = -1
        gene.loc[gene['chrstart'] >= 999999999, ['chrstart']] = -1



        #merge gene and uniprot
        merged = pd.merge(uniprot, gene, how='left', left_on='geneid', right_on='uid')

        #add the expression data
        uni_map = pd.read_csv(gene_map)
        uni_map.columns = ['idx', 'Entry','source', 'ensembl_id']
        uni_map['ensembl_id'] = uni_map['ensembl_id'].str.lower()
        oriented_df.index = oriented_df.index.str.lower()
        expression = pd.merge(uni_map[['Entry', 'ensembl_id']], oriented_df, how='left', left_on='ensembl_id', right_index=True)

        #clear memory
        del(uniprot)
        del(gene)
        del(oriented_df)
        del(expdf)
        del(uni_map)


        merged_final = pd.merge(merged, expression, how='left', on='Entry')
        del merged_final['geneid']

        temp=merged_final.drop(columns=['Mapped PubMed ID'])
        temp['chromosome']=temp['chromosome'].astype(str)
        temp.dropna(how='all',axis=1,inplace=True)
        temp.to_parquet(datapath+'/merged_final.parquet')  

        print('saved merged parquet in: '+datapath+'/merged_final.parquet')  
    return datapath+'/merged_final.parquet'
